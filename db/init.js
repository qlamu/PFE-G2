let res = [
  db.users.createIndex({ email: 1 }, { unique: true }),
  db.users.insertOne({
    firstName: "Super",
    lastName: "Admin",
    email: "admin@example.com",
    role: "admin",
    password: "$2b$10$Nj4A3WrrymBgeDj7p9yrruMDnWOG4E8z43L3r33WdNax9wOuPslDm" // "admin-g2" before hash
  })
]

printjson(res)