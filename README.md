# PFE G2

A multiple choice question app built with Fastify, MongoDB and VueJS allowing students to register themselves and pass exams.

To run the project locally, run `docker-compose up` in a terminal.

The API will then be availabe on port 8000 and the front end on port 80, you can modify those in the `docker-compose.yml`.

## API: Fastify + MongoDB

Most of the configuration is retrieved from the environment, so it is quite easy to change it directly in the file `docker-compose.yml`.

The app comes with tests powered by `jest` and, they can be run like so:

If the docker is up: `docker exec g2API /bin/sh -c "cd /usr/src/app && npm install --also=dev && npm run test"`

Locally:
```
cd back
npm install
npm run test
```
