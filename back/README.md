# Back

## Project setup

Install the dependencies:
```
npm install
```

Create a `.env` file with you development config:
```
PORT="8000"
HOST="localhost"
JWT_SECRET="aHR0cHM6Ly9naXRsYWIuY29tL3FsYW11L1BGRS1HMg=="
MONGO_HOST="localhost"
MONGO_DB="pfe-g2"
MONGO_PORT="27017"
```


## Run
```
npm run start
```

## Test
```
npm run test
```