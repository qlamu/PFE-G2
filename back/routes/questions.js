const { checkJWT, checkProfessorRights } = require("../utils/middlewares.js");
const mongodb = require("mongodb");

async function questionsRouter(fastify, options) {
  fastify.put(
    "/:id_exam",
    {
      preHandler: [checkJWT, checkProfessorRights],
      schema: {
        summary: "(Professor) Add a new question to the exam id_exam",
        tags: [{ name: "Exam Questions" }],
        security: [{ JWT: [] }],
        params: {
          type: "object",
          properties: {
            id_exam: { type: "string" },
          },
        },
        body: { $ref: "http://url.com/add-exam.json#/properties/questions/items" },
        response: {
          201: {
            description: "Successful insert, returns the id of the new exam",
            type: "object",
            properties: {
              message: { $ref: "http://url.com/default.json#/properties/message" },
              data: {
                type: "object",
                properties: {
                  id_question: {
                    type: "string",
                  },
                },
              },
            },
          },
          400: {
            description: "Schema invalid or could not create the exam",
            $ref: "http://url.com/default.json",
          },
        },
      },
    },
    (req, res) => {
      const questionID = new mongodb.ObjectId();

      fastify.mongo.db
        .collection("exams")
        .updateOne(
          {
            _id: mongodb.ObjectId(req.params.id_exam),
          },
          {
            $push: { questions: { id_question: questionID, ...req.body } },
          },
          {
            upsert: true,
          }
        )
        .then((s) =>
          res.code(201).send({
            message: "OK, Question created",
            data: {
              id_question: questionID,
            },
          })
        )
        .catch((err) => {
          console.log(err);
          res.code(400).send({
            message: "ERR " + err,
          });
        });
    }
  );

  fastify.delete(
    "/delete/:id_exam/:id_question",
    {
      preHandler: [checkJWT, checkProfessorRights],
      schema: {
        summary: "(Professor) Delete the question id_question that is part of the exam id_exam",
        tags: [{ name: "Exam Questions" }],
        security: [{ JWT: [] }],
        params: {
          type: "object",
          properties: {
            id_exam: { type: "string" },
            id_question: { type: "string" },
          },
        },
        response: {
          201: {
            description: "Successfully updated the exam id_exam",
            $ref: "http://url.com/default.json",
          },
          400: {
            description: "Schema invalid or could not create the exam",
            $ref: "http://url.com/default.json",
          },
        },
      },
    },
    (req, res) => {
      fastify.mongo.db
        .collection("exams")
        .updateOne(
          {
            _id: mongodb.ObjectId(req.params.id_exam),
          },
          {
            $pull: {
              questions: {
                $or: [
                  { id_question: req.params.id_question },
                  { id_question: mongodb.ObjectId(req.params.id_question) },
                ],
              },
            },
          }
        )
        .then(() =>
          res.code(200).send({
            message: "OK, Question delete",
          })
        )
        .catch((err) => {
          console.log(err);
          res.code(400).send({
            message: "ERR " + err,
          });
        });
    }
  );

  fastify.patch(
    "/update/:id_exam/:id_question",
    {
      preHandler: [checkJWT, checkProfessorRights],
      schema: {
        summary: "(Professor) Update the question id_question that is part of the exam id_exam",
        tags: [{ name: "Exam Questions" }],
        security: [{ JWT: [] }],
        params: {
          type: "object",
          properties: {
            id_exam: { type: "string" },
            id_question: { type: "string" },
          },
        },
        body: { $ref: "http://url.com/add-exam.json#/properties/questions/items" },
        response: {
          201: {
            description: "Successfully updated the exam id_exam",
            $ref: "http://url.com/default.json",
          },
          400: {
            description: "Schema invalid or could not create the exam",
            $ref: "http://url.com/default.json",
          },
        },
      },
    },
    (req, res) => {
      const update = Object.fromEntries(
        Object.entries(req.body).map(([k, v]) => ["questions.$[qid]." + k, v])
      );

      fastify.mongo.db
        .collection("exams")
        .updateOne(
          {
            _id: mongodb.ObjectId(req.params.id_exam),
          },
          {
            $set: {
              ...update, // don't pass ...req.body here, or it will replace and not update existing document
            },
          },
          {
            arrayFilters: [{ "qid.id_question": mongodb.ObjectId(req.params.id_question) }],
            upsert: true,
          }
        )
        .then(() =>
          res.code(200).send({
            message: "OK, Question updated",
          })
        )
        .catch((err) => {
          console.log(err);
          res.code(400).send({
            message: "ERR " + err,
          });
        });
    }
  );
}

module.exports = questionsRouter;
