const { checkAdminRights, checkJWT } = require("../utils/middlewares.js");
const mongodb = require("mongodb");

async function adminRouter(fastify, options) {
  fastify.get(
    "/users",
    {
      preHandler: [checkJWT, checkAdminRights],
      schema: {
        summary: "(Admin) Get the list of users",
        tags: [{ name: "Admin" }],
        security: [{ JWT: [] }],
        response: {
          200: {
            description: "Successfully retrieved the list of users",
            type: "object",
            properties: {
              message: { $ref: "http://url.com/default.json#/properties/message" },
              data: {
                type: "object",
                properties: {
                  users: {
                    type: "array",
                    items: {
                      type: "object",
                      properties: {
                        _id: { type: "string" },
                        firstName: { type: "string" },
                        lastName: { type: "string" },
                        email: { type: "string", format: "email" },
                        role: { type: "string", enum: ["student", "professor", "admin"] },
                        extraTime: { type: "number" },
                        participatedIn: { type: "array", items: { type: "string" } },
                      },
                    },
                  },
                },
              },
            },
          },
          400: {
            description: "Error",
            $ref: "http://url.com/default.json",
          },
        },
      },
    },
    (req, res) => {
      fastify.mongo.db
        .collection("users")
        .find(
          {},
          {
            password: 0,
          }
        )
        .toArray()
        .then((users) => {
          res.code(200).send({
            message: "OK, Users retrieved",
            data: {
              users: users,
            },
          });
        })
        .catch((err) => {
          console.log(err);
          res.code(400).send({
            message: "ERR " + err,
          });
        });
    }
  );

  fastify.patch(
    "/users/update/:id_user",
    {
      preHandler: [checkJWT, checkAdminRights],
      schema: {
        summary: "(Admin) Edit and existing user",
        tags: [{ name: "Admin" }],
        security: [{ JWT: [] }],
        params: {
          type: "object",
          properties: {
            id_user: { type: "string" },
          },
        },
        body: {
          type: "object",
          properties: {
            firstName: { type: "string" },
            lastName: { type: "string" },
            email: { type: "string", format: "email" },
            role: { type: "string", enum: ["student", "professor", "admin"] },
            extraTime: { type: "number" },
            participatedIn: { type: "array", items: { type: "string" } },
          },
        },
        response: {
          200: {
            description: "Successfully updated the user id_user",
            $ref: "http://url.com/default.json",
          },
          400: {
            description: "Error",
            $ref: "http://url.com/default.json",
          },
        },
      },
    },
    (req, res) => {
      fastify.mongo.db
        .collection("users")
        .updateOne(
          {
            _id: mongodb.ObjectId(req.params.id_user),
          },
          {
            $set: { ...req.body },
          }
        )
        .then(() =>
          res.code(200).send({
            message: "OK, Role updated",
          })
        )
        .catch((err) => {
          console.log(err);
          res.code(400).send({
            message: "ERR " + err,
          });
        });
    }
  );
}

module.exports = adminRouter;
