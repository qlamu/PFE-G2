const { checkJWT } = require("../utils/middlewares.js");
const mongodb = require("mongodb");

async function answersRouter(fastify, options) {
  fastify.put(
    "/:id_exam/:id_question",
    {
      preHandler: [checkJWT],
      schema: {
        summary: "Add on answer to the question id_question that is part of the exam id_exam",
        tags: [{ name: "Exam Answers" }],
        security: [{ JWT: [] }],
        params: {
          type: "object",
          properties: {
            id_exam: { type: "string" },
            id_question: { type: "string" },
          },
        },
        body: {
          type: "array",
          items: { type: "string" },
        },
        response: {
          200: {
            description: "Success",
            $ref: "http://url.com/default.json",
          },
          400: {
            description: "Error",
            $ref: "http://url.com/default.json",
          },
        },
      },
    },
    async (req, res) => {
      const currExam = await fastify.mongo.db
        .collection("exams")
        .findOne({ _id: mongodb.ObjectId(req.params.id_exam) });

      const studentTimeAway = currExam.studentTimeAway?.[req.jwt.userID] || 0;
      const maxTimeAway = currExam.maxTimeAway || 0;

      if (studentTimeAway <= maxTimeAway) {
        fastify.mongo.db
          .collection("exams")
          .updateOne(
            {
              _id: mongodb.ObjectId(req.params.id_exam),
            },
            {
              $set: {
                ["questions.$[qid].studentAnswers." + req.jwt.userID]: req.body,
              },
            },
            {
              arrayFilters: [{ "qid.id_question": mongodb.ObjectId(req.params.id_question) }],
              upsert: true,
            }
          )
          .then(() =>
            res.code(200).send({
              message: "OK, Answer updated",
            })
          )
          .catch((err) => {
            console.log(err);
            res.code(400).send({
              message: "ERR, " + err,
            });
          });
      } else {
        res.code(400).send({
          message: "ERR, Student time away is over the specified limit",
        });
      }
    }
  );
}

module.exports = answersRouter;
