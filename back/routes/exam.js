const moment = require("moment");
const { checkJWT, checkProfessorRights } = require("../utils/middlewares.js");
const mongodb = require("mongodb");

async function examRouter(fastify, options) {
  fastify.get(
    "/",
    {
      preHandler: [checkJWT, checkProfessorRights],
      schema: {
        summary: "(Professor) Get all the exams of the authenticated professor",
        tags: [{ name: "Exam" }],
        security: [{ JWT: [] }],
        response: {
          200: {
            description: "A list with the exams of the authenticated user",
            type: "object",
            properties: {
              message: { $ref: "http://url.com/default.json#/properties/message" },
              data: {
                type: "object",
                properties: {
                  exams: {
                    type: "array",
                    items: { $ref: "http://url.com/exam.json" },
                  },
                },
              },
            },
          },
          400: {
            description: "Error",
            $ref: "http://url.com/default.json",
          },
        },
      },
    },
    (req, res) => {
      fastify.mongo.db
        .collection("exams")
        .find(
          {
            ownerId: req.jwt.userID,
          },
          {
            _id: 1,
            title: 1,
            startDate: 1,
          }
        )
        .toArray()
        .then((exams) =>
          res.code(200).send({
            message: "OK, Exams queried",
            data: {
              exams: exams,
            },
          })
        )
        .catch((err) => {
          console.log(err);
          res.code(400).send({
            message: { type: "string" },
          });
        });
    }
  );

  fastify.get(
    "/:id_exam",
    {
      preHandler: [checkJWT],
      schema: {
        summary: "Get a specific exam, if student: without answers, if professor: full",
        tags: [{ name: "Exam" }],
        security: [{ JWT: [] }],
        params: {
          type: "object",
          properties: {
            id_exam: { type: "string" },
          },
        },
        response: {
          200: {
            description:
              "The exam with the specified id, for students the exam doesn't contain everything.",
            type: "object",
            properties: {
              message: { $ref: "http://url.com/default.json#/properties/message" },
              data: {
                type: "object",
                properties: {
                  exam: { $ref: "http://url.com/exam.json" },
                },
              },
            },
          },
          403: {
            description: "(Student) The exam is not accessible at this time.",
            $ref: "http://url.com/default.json",
          },
          404: {
            description: "The exam does not exist.",
            $ref: "http://url.com/default.json",
          },
        },
      },
    },
    async (req, res) => {
      if (["professor", "admin"].includes(req.jwt.role)) {
        try {
          const exam = await fastify.mongo.db
            .collection("exams")
            .findOne({ _id: mongodb.ObjectId(req.params.id_exam) });

          res.code(200).send({
            message: "OK, Exam queried",
            data: {
              exam: exam,
            },
          });
        } catch (err) {
          res.code(404).send({
            message: "ERR, Exam with specified ID not found",
          });
        }
      } else {
        // Student
        try {
          const user = await fastify.mongo.db
            .collection("users")
            .findOne({ _id: mongodb.ObjectId(req.jwt.userID) });

          const exam = await fastify.mongo.db.collection("exams").findOne(
            { _id: mongodb.ObjectId(req.params.id_exam) },
            {
              projection: {
                _id: 0,
                title: 1,
                startDate: 1,
                duration: 1,
                "questions.id_question": 1,
                "questions.question": 1,
                "questions.availableAnswers": 1,
                ["questions.studentAnswers." + req.jwt.userID]: 1,
                ["studentTimeAway." + req.jwt.userID]: 1,
              },
            }
          );

          exam.startDate = moment(exam.startDate);
          exam.endDate = moment(exam.startDate).add(
            exam.duration + exam.duration * (user.extraTime / 100),
            "minutes"
          );

          if (!(moment().isBefore(exam.startDate) || moment().isAfter(exam.endDate))) {
            res.code(200).send({
              message: "OK, Exam queried",
              data: {
                exam: exam,
              },
            });
          } else {
            res.code(403).send({
              message: "ERR, The exam is not up at this time.",
            });
          }
        } catch (err) {
          res.code(404).send({
            message: "ERR, Exam with specified ID not found",
          });
        }
      }
    }
  );

  fastify.put(
    "/create",
    {
      preHandler: [checkJWT, checkProfessorRights],
      schema: {
        summary: "(Professor) Create a new exam",
        tags: [{ name: "Exam" }],
        security: [{ JWT: [] }],
        body: { $ref: "http://url.com/add-exam.json" },
        response: {
          201: {
            description: "Successful insert, returns the id of the new exam",
            type: "object",
            properties: {
              message: { $ref: "http://url.com/default.json#/properties/message" },
              data: {
                type: "object",
                properties: {
                  id_exam: {
                    type: "string",
                  },
                },
              },
            },
          },
          400: {
            description: "Schema invalid or could not create the exam",
            $ref: "http://url.com/default.json",
          },
        },
      },
    },
    (req, res) => {
      if (req.body.hasOwnProperty("questions")) {
        req.body.questions.forEach((question) => {
          question.id_question = new mongodb.ObjectId();
        });
      }

      fastify.mongo.db
        .collection("exams")
        .insertOne({
          ...req.body,
          ownerId: req.jwt.userID,
        })
        .then((s) =>
          res.code(201).send({
            message: "OK, Exam created",
            data: {
              id_exam: s.insertedId,
            },
          })
        )
        .catch((err) => {
          res.code(400).send({
            message: "ERR, " + err,
          });
          console.log(err);
        });
    }
  );

  fastify.patch(
    "/update/:id_exam",
    {
      preHandler: [checkJWT, checkProfessorRights],
      schema: {
        summary: "(Professor) Update an existing exam",
        tags: [{ name: "Exam" }],
        security: [{ JWT: [] }],
        params: {
          type: "object",
          properties: {
            id_exam: { type: "string" },
          },
        },
        body: { $ref: "http://url.com/add-exam.json" },
        response: {
          200: {
            description: "Successfully updated the exam id_exam",
            $ref: "http://url.com/default.json",
          },
          400: {
            description: "Schema invalid or could not update the exam",
            $ref: "http://url.com/default.json",
          },
        },
      },
    },
    (req, res) => {
      fastify.mongo.db
        .collection("exams")
        .updateOne(
          {
            _id: mongodb.ObjectId(req.params.id_exam),
          },
          {
            $set: { ...req.body },
          }
        )
        .then(() =>
          res.code(200).send({
            message: "OK, Exam updated",
          })
        )
        .catch((err) => {
          res.code(400).send({
            message: "ERR, " + err,
          });
        });
    }
  );

  fastify.delete(
    "/delete/:id_exam",
    {
      preHandler: [checkJWT, checkProfessorRights],
      schema: {
        summary: "(Professor) Delete an exam",
        tags: [{ name: "Exam" }],
        security: [{ JWT: [] }],
        params: {
          type: "object",
          properties: {
            id_exam: { type: "string" },
          },
        },
        response: {
          200: {
            description: "Success",
            $ref: "http://url.com/default.json",
          },
          400: {
            description: "Error",
            $ref: "http://url.com/default.json",
          },
        },
      },
    },
    (req, res) => {
      fastify.mongo.db
        .collection("exams")
        .deleteOne({
          _id: mongodb.ObjectId(req.params.id_exam),
        })
        .then(() => {
          res.code(200).send({
            message: "OK, Exam deleted",
          });
        })
        .catch((err) => {
          console.log(err);
          res.code(400).send({
            message: "ERR " + err,
          });
        });
    }
  );

  fastify.post(
    "/timeAway/:id_exam",
    {
      preHandler: [checkJWT],
      schema: {
        summary: "Add time to the counter of the time out of tab for the current user",
        tags: [{ name: "Exam" }],
        security: [{ JWT: [] }],
        params: {
          type: "object",
          properties: {
            id_exam: { type: "string" },
          },
        },
        body: {
          type: "object",
          required: ["timeAway"],
          properties: {
            timeAway: { type: "number" },
          },
        },
        response: {
          200: {
            description: "Success",
            $ref: "http://url.com/default.json",
          },
          400: {
            description: "Error",
            $ref: "http://url.com/default.json",
          },
        },
      },
    },
    (req, res) => {
      fastify.mongo.db
        .collection("exams")
        .updateOne(
          {
            _id: mongodb.ObjectId(req.params.id_exam),
          },
          {
            $inc: {
              ["studentTimeAway." + req.jwt.userID]: req.body.timeAway,
            },
          }
        )
        .then(() =>
          res.code(200).send({
            message: "OK, Time away updated",
          })
        )
        .catch((err) => {
          console.log(err);
          res.code(400).send({
            message: "ERR " + err,
          });
        });
    }
  );
}

module.exports = examRouter;
