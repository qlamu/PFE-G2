const jwt = require("jsonwebtoken");
const { hashSync, compareSync } = require("bcrypt");

async function authRouter(fastify, options) {
  fastify.post(
    "/register",
    {
      schema: {
        summary: "Create a new user",
        tags: [{ name: "Auth" }],
        body: {
          type: "object",
          required: ["email", "password"],
          properties: {
            firstName: { type: "string" },
            lastName: { type: "string" },
            email: { type: "string", format: "email" },
            password: { type: "string" },
          },
        },
        response: {
          201: {
            description: "User created",
            $ref: "http://url.com/default.json"
          },
          400: {
            description: "Bad schema, or email address already in use",
            $ref: "http://url.com/default.json"
          },
        },
      },
    },
    (req, res) => {
      fastify.mongo.db
        .collection("users")
        .insertOne({
          ...req.body,
          password: hashSync(req.body.password, 10),
          role: "student",
          extraTime: 0,
        })
        .then(() => {
          res.code(201).send({
            message: "OK, User created",
          });
        })
        .catch((err) => {
          res.code(400).send({
            message:
              err.code == 11000 ? "ERR, The email address is already in use" : "ERR " + err.code,
          });
        });
    }
  );

  fastify.post(
    "/login",
    {
      schema: {
        summary: "Authenticate an existing user",
        tags: [{ name: "Auth" }],
        body: {
          type: "object",
          required: ["email", "password"],
          properties: {
            email: { type: "string", format: "email" },
            password: { type: "string" },
          },
        },
        response: {
          200: {
            description: "User created",
            type: "object",
            properties: {
              message: { $ref: "http://url.com/default.json#/properties/message" },
              data: {
                type: "object",
                properties: {
                  jwt: {
                    type: "string",
                    example: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9",
                  },
                  user: {
                    type: "object",
                    properties: {
                      firstName: { type: "string" },
                      lastName: { type: "string" },
                      email: { type: "string" },
                      role: { type: "string" },
                      firstName: { type: "string" },
                      extraTime: { type: "string" },
                    }
                  }
                },
              },
            },
          },
          400: {
            description: "Bad schema, or email address already in use",
            $ref: "http://url.com/default.json"
          },
        },
      },
    },
    async (req, res) => {
      const user = await fastify.mongo.db.collection("users").findOne({ email: req.body.email });

      if (user && compareSync(req.body.password, user.password)) {
        var token = jwt.sign({ userID: user._id, role: user.role }, process.env.JWT_SECRET);
        res.send({
          message: "OK, Logged in",
          data: {
            jwt: token,
            user: {
              _id: user._id,
              firstName: user.firstName,
              lastName: user.lastName,
              email: user.email,
              role: user.role,
              extraTime: user.extraTime,
            }
          },
        });
      } else {
        res.code(400).send({
          message: "ERR, User does not exist, or the password is invalid",
        });
      }
    }
  );
}

module.exports = authRouter;
