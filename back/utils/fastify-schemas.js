module.exports = [
  // Default response
  {
    $id: "http://url.com/default.json",
    type: "object",
    properties: {
      message: { type: "string"}
    }
  },

  // Full exam
  {
    $id: "http://url.com/exam.json",
    type: "object",
    properties: {
      _id: { type: "string" },
      ownerId: { type: "string" },
      title: { type: "string" },
      startDate: { type: "string" },
      duration: { type: "number" },
      maxTimeAway: { type: "number" },
      questions: {
        type: "array",
        items: {
          type: "object",
          properties: {
            id_question: { type: "string" },
            question: { type: "string" },
            availableAnswers: { type: "array", items: { type: "string" } },
            correctAnswers: { type: "array", items: { type: "string" } },
            studentAnswers: {
              type: "object",
              additionalProperties: {
                studentID: { type: "array", items: { type: "string" } },
              },
            },
          },
        },
      },
      studentTimeAway: {
        type: "object",
        properties: {
          studentID: { type: "number" },
        },
      },
    },
  },

  // Add / update eaxm
  {
    $id: "http://url.com/add-exam.json",
    type: "object",
    properties: {
      title: { type: "string" },
      startDate: { type: "string" },
      duration: { type: "number" },
      maxTimeAway: { type: "number" },
      questions: {
        type: "array",
        items: {
          type: "object",
          properties: {
            question: { type: "string" },
            availableAnswers: { type: "array", items: { type: "string" } },
            correctAnswers: { type: "array", items: { type: "string" } },
          },
        },
      },
    },
  }
]