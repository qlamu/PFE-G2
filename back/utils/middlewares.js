const jwt = require("jsonwebtoken");

/**
 * Check the provided jwt, block the access if invalid or continue to the
 * route otherwise, the data from the token is token is stored in req.jwt
 * @param {Request} req
 * @param {Response} res
 * @param {Function} next
 */
 exports.checkJWT = (req, res, next) => {
  const token = req.headers["authorization"]?.slice(7); // Remove "Bearer ";
  if (token == null) {
    res.code(401).send({
      message: "ERR, No JWT provided",
    });
  } else {
    jwt.verify(token, process.env.JWT_SECRET, (err, decoded) => {
      if (err) {
        res.code(401).send({
          message: "ERR, Invalid JWT",
        });
      } else {
        req.jwt = decoded;
        next();
      }
    });
  }
};

/**
 * Check if the right embedded in the jwt token are within
 * a specified array of roles and proceed if they are.
 * @param {Array} allowedArray
 * @returns
 */
const checkRights = (allowedArray) => {
  return (req, res, next) => {
    if (allowedArray.includes(req.jwt?.role)) {
      next();
    } else {
      res.code(401).send({
        message: "ERR, Access denied",
      });
    }
  };
};

exports.checkProfessorRights = checkRights(["professor", "admin"]);
exports.checkAdminRights = checkRights(["admin"]);
