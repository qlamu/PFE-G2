"use strict";

require("dotenv").config();

const fastify = require("fastify");
const schemas = require("./utils/fastify-schemas");

function build(testing = false) {
  const app = fastify();

  if (!testing) {
    app.register(require("fastify-mongodb"), {
      forceClose: true,
      url: `mongodb://${process.env.MONGO_HOST}:${process.env.MONGO_PORT}/${process.env.MONGO_DB}`,
    });
  }

  schemas.forEach((schema) => app.addSchema(schema));

  app.register(require("fastify-cors"), {});

  app.register(require("fastify-swagger"), {
    routePrefix: "/documentation",
    openapi: {
      info: {
        title: "QCM app documentation",
        version: "1.0.0",
      },
      components: {
        securitySchemes: {
          JWT: {
            type: "http",
            scheme: "bearer",
            bearerFormat: "JWT",
          },
        },
      },
    },
    staticCSP: true,
    transformStaticCSP: (header) => header,
    exposeRoute: true,
  });

  app.get("/", (req, res) => res.redirect("/documentation"));

  app.register(require("./routes/auth"));
  app.register(require("./routes/exam"), { prefix: "/exam" });
  app.register(require("./routes/questions"), { prefix: "/exam/questions" });
  app.register(require("./routes/answers"), { prefix: "/exam/answers" });
  app.register(require("./routes/admin"), { prefix: "/admin" });

  app.ready((err) => {
    if (err) throw err;
    app.swagger();
  });

  return app;
}

module.exports = build;

if (require.main === module) {
  const server = build();

  server.listen(process.env.PORT || 8000, process.env.HOST || "0.0.0.0", (err, address) => {
    if (err) {
      console.log(err);
      process.exit(1);
    }
    console.log("Ready, server listening on " + address);
  });
}
