describe("POST /register", () => {
  it("should fail with invalid body", async () => {
    const res = await app.inject({
      method: "POST",
      url: "/register",
      payload: {
        email: "hello@world.com",
      },
    });
    expect(res.statusCode).toEqual(400);
  });

  it("should create a new user if the email doesn't exist", async () => {
    const res = await global.app.inject({
      method: "POST",
      url: "/register",
      payload: {
        firstName: "hello",
        email: "hello@world.com",
        password: "secret",
      },
    });

    expect(res.statusCode).toEqual(201);
  });

  it("should NOT create a new user if the email already exist", async () => {
    const res = await global.app.inject({
      method: "POST",
      url: "/register",
      payload: {
        email: "student@example.com",
        password: "stPass",
      },
    });
    expect(res.statusCode).toEqual(400);
  });
});

describe("POST /login", () => {
  it("expects valid body schema", async () => {
    const res = await global.app.inject({
      method: "POST",
      url: "/login",
      payload: {
        firstName : "hello",
      }
    });
    expect(res.statusCode).toEqual(400);
  });

  it("expects to not login with invalid credentials", async () => {
    const res = await global.app.inject({
      method: "POST",
      url: "/login",
      payload: {
        email : "student@example.com",
        password : "wrongPassword"
      }
    });
    expect(res.statusCode).toEqual(400);
  });

  it("expects to login with valid credentials", async () => {
    const res = await global.app.inject({
      method: "POST",
      url: "/login",
      payload: {
        email : "student@example.com",
        password : "stPass"
      }
    });
    expect(res.statusCode).toEqual(200);
    expect(res.json().data);
    expect(res.json().data.jwt);
  });
});
