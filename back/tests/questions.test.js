describe("PUT /:id_exam", () => {
  let professorToken;

  beforeAll(async () => {
    var res = await global.app.inject({
      method: "POST",
      url: "/login",
      payload: {
        email: "professor@example.com",
        password: "prPass",
      },
    });
    expect(res.statusCode).toEqual(200);
    professorToken = res.json().data.jwt;
  });

  it("should create a new question", async () => {
    // Create exam
    var res = await global.app.inject({
      method: "PUT",
      url: "/exam/create",
      headers: {
        Authorization: "Bearer " + professorToken,
      },
      payload: {
        title: "Test insert",
        startDate: "2111-10-05T14:48:00.000Z",
        duration: 120,
      },
    });
    expect(res.statusCode).toEqual(201);
    const examID = res.json().data.id_exam;

    // Add question
    res = await global.app.inject({
      method: "PUT",
      url: `/exam/questions/${examID}`,
      headers: {
        Authorization: "Bearer " + professorToken,
      },
      payload: {
        question: "2 + 2 * 2 = ?",
        availableAnswers: ["6", "8"],
        correctAnswers: ["6"],
      },
    });
    expect(res.statusCode).toEqual(201);

    // Get exam, check if the question is there
    res = await global.app.inject({
      method: "GET",
      url: `/exam/${examID}`,
      headers: {
        Authorization: "Bearer " + professorToken,
      },
    });
    expect(res.statusCode).toEqual(200);
    expect(res.json().data.exam.questions.length).toEqual(1);
  });
});

describe("DELETE /delete/:id_exam/:id_question", () => {
  let professorToken;

  beforeAll(async () => {
    var res = await global.app.inject({
      method: "POST",
      url: "/login",
      payload: {
        email: "professor@example.com",
        password: "prPass",
      },
    });
    expect(res.statusCode).toEqual(200);
    professorToken = res.json().data.jwt;
  });

  it("should update delete question", async () => {
    // Create exam
    var res = await global.app.inject({
      method: "PUT",
      url: "/exam/create",
      headers: {
        Authorization: "Bearer " + professorToken,
      },
      payload: {
        title: "Test insert",
        startDate: "2111-10-05T14:48:00.000Z",
        duration: 120,
      },
    });
    expect(res.statusCode).toEqual(201);
    const examID = res.json().data.id_exam;

    // Add question
    res = await global.app.inject({
      method: "PUT",
      url: `/exam/questions/${examID}`,
      headers: {
        Authorization: "Bearer " + professorToken,
      },
      payload: {
        question: "4 + 4 * 4 = ?",
        availableAnswers: ["32", "20"],
        correctAnswers: ["32"],
      },
    });
    expect(res.statusCode).toEqual(201);
    const questionID = res.json().data.id_question;

    // Delete question
    res = await global.app.inject({
      method: "DELETE",
      url: `/exam/questions/delete/${examID}/${questionID}`,
      headers: {
        Authorization: "Bearer " + professorToken,
      }
    });
    expect(res.statusCode).toEqual(200);

    // Get exam, check if the question is updated
    res = await global.app.inject({
      method: "GET",
      url: `/exam/${examID}`,
      headers: {
        Authorization: "Bearer " + professorToken,
      },
    });
    expect(res.statusCode).toEqual(200);
    expect(res.json().data.exam.questions.length).toEqual(0);
  });
});

describe("PATCH /update/:id_exam/:id_question", () => {
  let professorToken;

  beforeAll(async () => {
    var res = await global.app.inject({
      method: "POST",
      url: "/login",
      payload: {
        email: "professor@example.com",
        password: "prPass",
      },
    });
    expect(res.statusCode).toEqual(200);
    professorToken = res.json().data.jwt;
  });

  it("should update a question", async () => {
    // Create exam
    var res = await global.app.inject({
      method: "PUT",
      url: "/exam/create",
      headers: {
        Authorization: "Bearer " + professorToken,
      },
      payload: {
        title: "Test insert",
        startDate: "2111-10-05T14:48:00.000Z",
        duration: 120,
      },
    });
    expect(res.statusCode).toEqual(201);
    const examID = res.json().data.id_exam;

    // Add question
    res = await global.app.inject({
      method: "PUT",
      url: `/exam/questions/${examID}`,
      headers: {
        Authorization: "Bearer " + professorToken,
      },
      payload: {
        question: "4 + 4 * 4 = ?",
        availableAnswers: ["32", "20"],
        correctAnswers: ["32"],
      },
    });
    expect(res.statusCode).toEqual(201);
    const questionID = res.json().data.id_question;

    // Update question
    res = await global.app.inject({
      method: "PATCH",
      url: `/exam/questions/update/${examID}/${questionID}`,
      headers: {
        Authorization: "Bearer " + professorToken,
      },
      payload: {
        correctAnswers: ["20"],
      },
    });
    expect(res.statusCode).toEqual(200);

    // Get exam, check if the question is updated
    res = await global.app.inject({
      method: "GET",
      url: `/exam/${examID}`,
      headers: {
        Authorization: "Bearer " + professorToken,
      },
    });
    expect(res.statusCode).toEqual(200);
    expect(res.json().data.exam.questions[0].correctAnswers).toEqual(["20"]);
  });
});
