const jwt = require("jsonwebtoken");

describe("PUT /:id_exam/:id_question", () => {
  let studentToken;
  let professorToken;

  beforeAll(async () => {
    var res = await global.app.inject({
      method: "POST",
      url: "/login",
      payload: {
        email: "student@example.com",
        password: "stPass",
      },
    });
    expect(res.statusCode).toEqual(200);
    studentToken = res.json().data.jwt;

    res = await global.app.inject({
      method: "POST",
      url: "/login",
      payload: {
        email: "professor@example.com",
        password: "prPass",
      },
    });
    expect(res.statusCode).toEqual(200);
    professorToken = res.json().data.jwt;
  });

  it("should add an answer", async () => {
    // Create exam
    var res = await global.app.inject({
      method: "PUT",
      url: "/exam/create",
      headers: {
        Authorization: "Bearer " + professorToken,
      },
      payload: {
        title: "Test insert",
        startDate: "2111-10-05T14:48:00.000Z",
        duration: 120,
      },
    });
    expect(res.statusCode).toEqual(201);
    const examID = res.json().data.id_exam;

    // Add question
    res = await global.app.inject({
      method: "PUT",
      url: `/exam/questions/${examID}`,
      headers: {
        Authorization: "Bearer " + professorToken,
      },
      payload: {
        question: "4 + 4 * 4 = ?",
        availableAnswers: ["32", "20"],
        correctAnswers: ["20"],
      },
    });
    expect(res.statusCode).toEqual(201);
    const questionID = res.json().data.id_question;

    // Add answer to question
    res = await global.app.inject({
      method: "PUT",
      url: `/exam/answers/${examID}/${questionID}`,
      headers: {
        Authorization: "Bearer " + studentToken,
      },
      payload: ["20"],
    });
    expect(res.statusCode).toEqual(200);

    // Get exam, check the student answer
    res = await global.app.inject({
      method: "GET",
      url: `/exam/${examID}`,
      headers: {
        Authorization: "Bearer " + professorToken,
      },
    });
    expect(res.statusCode).toEqual(200);
    const studentID = jwt.verify(studentToken, process.env.JWT_SECRET).userID;
    expect(res.json().data.exam.questions[0].studentAnswers[studentID]).toEqual(["20"]);
  });
});
