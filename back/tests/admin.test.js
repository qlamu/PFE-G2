const jwt = require("jsonwebtoken");

describe("/admin", () => {
  let studentToken;
  let adminToken;

  beforeAll(async () => {
    var res = await global.app.inject({
      method: "POST",
      url: "/login",
      payload: {
        email: "student@example.com",
        password: "stPass",
      },
    });
    expect(res.statusCode).toEqual(200);
    studentToken = res.json().data.jwt;

    res = await global.app.inject({
      method: "POST",
      url: "/login",
      payload: {
        email: "admin@example.com",
        password: "adPass",
      },
    });
    expect(res.statusCode).toEqual(200);
    adminToken = res.json().data.jwt;
  });

  it("should return multiple users", async () => {
    const res = await global.app.inject({
      method: "GET",
      url: "/admin/users",
      headers: {
        Authorization: "Bearer " + adminToken,
      }
    });
    expect(res.statusCode).toEqual(200);
    expect(res.json().data.users.length).toBeGreaterThanOrEqual(3);
  });


  it("should update the role of an user", async () => {
    const studentID = jwt.verify(studentToken, process.env.JWT_SECRET).userID;

    var res = await global.app.inject({
      method: "PATCH",
      url: `/admin/users/update/${studentID}`,
      headers: {
        Authorization: "Bearer " + adminToken,
      },
      payload: {
        firstName: "New name"
      }
    });
    expect(res.statusCode).toEqual(200);

    res = await global.app.inject({
      method: "GET",
      url: "/admin/users",
      headers: {
        Authorization: "Bearer " + adminToken,
      }
    });
    expect(res.statusCode).toEqual(200);

    const user = res.json().data.users.find(user => user._id == studentID)
    expect(user.firstName).toEqual("New name");
  });

});
