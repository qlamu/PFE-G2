const mongodb = require("mongodb");
const jwt = require("jsonwebtoken");

describe("GET /exam", () => {
  let studentToken;
  let professorToken;

  beforeAll(async () => {
    var res = await global.app.inject({
      method: "POST",
      url: "/login",
      payload: {
        email: "student@example.com",
        password: "stPass",
      },
    });
    expect(res.statusCode).toEqual(200);
    studentToken = res.json().data.jwt;

    res = await global.app.inject({
      method: "POST",
      url: "/login",
      payload: {
        email: "professor@example.com",
        password: "prPass",
      },
    });
    expect(res.statusCode).toEqual(200);
    professorToken = res.json().data.jwt;
  });

  it("should not allow students", async () => {
    const res = await global.app.inject({
      method: "GET",
      url: "/exam",
      headers: {
        Authorization: "Bearer " + studentToken,
      },
    });
    expect(res.statusCode).toEqual(401);
  });

  it("should allow professors", async () => {
    const res = await global.app.inject({
      method: "GET",
      url: "/exam",
      headers: {
        Authorization: "Bearer " + professorToken,
      },
    });
    expect(res.statusCode).not.toEqual(401);
  });

  it("expects to return exams", async () => {
    await connection.db().collection("exams").deleteMany({});

    var res = await global.app.inject({
      method: "GET",
      url: "/exam",
      headers: {
        Authorization: "Bearer " + professorToken,
      },
    });
    expect(res.statusCode).toEqual(200);
    expect(res.json().data.exams.length).toEqual(0);

    // Add exam
    res = await global.app.inject({
      method: "PUT",
      url: "/exam/create",
      headers: {
        Authorization: "Bearer " + professorToken,
      },
      payload: {
        title: "Test insert",
        startDate: "2222-10-05T14:48:00.000Z",
        duration: 120,
      },
    });

    // Check new number of exams
    res = await global.app.inject({
      method: "GET",
      url: "/exam",
      headers: {
        Authorization: "Bearer " + professorToken,
      },
    });
    expect(res.statusCode).toEqual(200);
    expect(res.json().data.exams.length).toBeGreaterThanOrEqual(1);
  });
});

describe("GET /exam/:id", () => {
  let studentToken;
  let professorToken;

  beforeAll(async () => {
    var res = await global.app.inject({
      method: "POST",
      url: "/login",
      payload: {
        email: "student@example.com",
        password: "stPass",
      },
    });
    expect(res.statusCode).toEqual(200);
    studentToken = res.json().data.jwt;

    res = await global.app.inject({
      method: "POST",
      url: "/login",
      payload: {
        email: "professor@example.com",
        password: "prPass",
      },
    });
    expect(res.statusCode).toEqual(200);
    professorToken = res.json().data.jwt;
  });

  it("expects to refuse access to exam", async () => {
    // Insert a future exam
    var res = await global.app.inject({
      method: "PUT",
      url: "/exam/create",
      headers: {
        Authorization: "Bearer " + professorToken,
      },
      payload: {
        title: "Test insert",
        startDate: "2121-04-30T20:05:14.257Z",
        duration: 120,
      },
    });
    expect(res.statusCode).toEqual(201);
    const examID = res.json().data.id_exam;

    res = await global.app.inject({
      method: "GET",
      url: `/exam/${examID}`,
      headers: {
        Authorization: "Bearer " + studentToken,
      },
    });
    expect(res.statusCode).toEqual(403);
  });

  it("expects to access running exam", async () => {
    // Insert a current exam
    var res = await global.app.inject({
      method: "PUT",
      url: "/exam/create",
      headers: {
        Authorization: "Bearer " + professorToken,
      },
      payload: {
        title: "Test insert",
        startDate: new Date().toISOString(),
        duration: 120,
      },
    });
    expect(res.statusCode).toEqual(201);
    const examID = res.json().data.id_exam;

    res = await global.app.inject({
      method: "GET",
      url: `/exam/${examID}`,
      headers: {
        Authorization: "Bearer " + studentToken,
      },
    });
    expect(res.statusCode).toEqual(200);
    expect(res.json()).toBeDefined();
  });
});

describe("PUT /exam/create", () => {
  let studentToken;
  let professorToken;

  beforeAll(async () => {
    var res = await global.app.inject({
      method: "POST",
      url: "/login",
      payload: {
        email: "student@example.com",
        password: "stPass",
      },
    });
    expect(res.statusCode).toEqual(200);
    studentToken = res.json().data.jwt;

    res = await global.app.inject({
      method: "POST",
      url: "/login",
      payload: {
        email: "professor@example.com",
        password: "prPass",
      },
    });
    expect(res.statusCode).toEqual(200);
    professorToken = res.json().data.jwt;
  });

  it("should not allow students", async () => {
    const res = await global.app.inject({
      method: "PUT",
      url: "/exam/create",
      headers: {
        Authorization: "Bearer " + studentToken,
      },
      payload: {
        title: "Test insert",
        startDate: "2111-10-05T14:48:00.000Z",
        duration: 120,
      },
    });
    expect(res.statusCode).toEqual(401);
  });

  it("should allow professors", async () => {
    const res = await global.app.inject({
      method: "PUT",
      url: "/exam/create",
      headers: {
        Authorization: "Bearer " + professorToken,
      },
      payload: {
        title: "Test insert",
        startDate: "2111-10-05T14:48:00.000Z",
        duration: 120,
      },
    });
    expect(res.statusCode).toEqual(201);
  });
});

describe("PATCH /update/:id_exam", () => {
  let professorToken;

  beforeAll(async () => {
    const res = await global.app.inject({
      method: "POST",
      url: "/login",
      payload: {
        email: "professor@example.com",
        password: "prPass",
      },
    });
    expect(res.statusCode).toEqual(200);
    professorToken = res.json().data.jwt;
  });

  it("expects to update the exam", async () => {
    var res = await global.app.inject({
      method: "PUT",
      url: "/exam/create",
      headers: {
        Authorization: "Bearer " + professorToken,
      },
      payload: {
        title: "Test insert",
        startDate: "2222-10-05T14:48:00.000Z",
        duration: 120,
      },
    });
    expect(res.statusCode).toEqual(201);
    expect(res.json().data.id_exam).toBeDefined();
    const examID = res.json().data.id_exam;

    res = await global.app.inject({
      method: "PATCH",
      url: `/exam/update/${examID}`,
      headers: {
        Authorization: "Bearer " + professorToken,
      },
      payload: {
        title: "New Title",
        startDate: "2222-10-05T14:48:00.000Z",
        duration: 60,
      },
    });
    expect(res.statusCode).toEqual(200);

    res = await global.app.inject({
      method: "GET",
      url: `/exam/${examID}`,
      headers: {
        Authorization: "Bearer " + professorToken,
      },
    });
    expect(res.statusCode).toEqual(200);
    expect(res.json().data.exam.title).toEqual("New Title");
    expect(res.json().data.exam.duration).toEqual(60);
  });
});

describe("DELETE /delete/:id_exam", () => {
  let professorToken;

  beforeAll(async () => {
    res = await global.app.inject({
      method: "POST",
      url: "/login",
      payload: {
        email: "professor@example.com",
        password: "prPass",
      },
    });
    expect(res.statusCode).toEqual(200);
    professorToken = res.json().data.jwt;
  });

  it("delete an exam", async () => {
    // Create an exam
    var res = await global.app.inject({
      method: "PUT",
      url: "/exam/create",
      headers: {
        Authorization: "Bearer " + professorToken,
      },
      payload: {
        title: "Test delete",
        startDate: new Date().toISOString(),
        duration: 120,
      },
    });
    expect(res.statusCode).toEqual(201);
    const examID = res.json().data.id_exam;

    // Delete it
    res = await global.app.inject({
      method: "DELETE",
      url: `/exam/delete/${examID}`,
      headers: {
        Authorization: "Bearer " + professorToken,
      },
    });
    expect(res.statusCode).toEqual(200);

    // Try to get it
    res = await global.app.inject({
      method: "GET",
      url: `/exam/${examID}`,
      headers: {
        Authorization: "Bearer " + professorToken,
      },
    });
    expect(res.statusCode).toEqual(404);
  });
});

describe("POST /timeAway/:id_exam", () => {
  let studentToken;
  let professorToken;

  beforeAll(async () => {
    var res = await global.app.inject({
      method: "POST",
      url: "/login",
      payload: {
        email: "student@example.com",
        password: "stPass",
      },
    });
    expect(res.statusCode).toEqual(200);
    studentToken = res.json().data.jwt;

    res = await global.app.inject({
      method: "POST",
      url: "/login",
      payload: {
        email: "professor@example.com",
        password: "prPass",
      },
    });
    expect(res.statusCode).toEqual(200);
    professorToken = res.json().data.jwt;
  });

  it("update the time away of the current user", async () => {
    // Create an exam
    var res = await global.app.inject({
      method: "PUT",
      url: "/exam/create",
      headers: {
        Authorization: "Bearer " + professorToken,
      },
      payload: {
        title: "Test insert",
        startDate: new Date().toISOString(),
        duration: 120,
      },
    });
    expect(res.statusCode).toEqual(201);
    const examID = res.json().data.id_exam;

    // Add time away
    res = await global.app.inject({
      method: "POST",
      url: `/exam/timeAway/${examID}`,
      headers: {
        Authorization: "Bearer " + studentToken,
      },
      payload: {
        timeAway: 10,
      },
    });
    expect(res.statusCode).toEqual(200);

    const exam = await connection
      .db()
      .collection("exams")
      .findOne({ _id: mongodb.ObjectId(examID) });
    const studentID = jwt.verify(studentToken, process.env.JWT_SECRET).userID;
    expect(exam.studentTimeAway[studentID]).toEqual(10);
  });
});
