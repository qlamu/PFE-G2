const { MongoClient } = require("mongodb");
const build = require("../api.js");
const { hashSync } = require("bcrypt");

/**
 * Setup a mock database and start the server
 */
beforeAll(async () => {
  process.env.JWT_SECRET = "test_jwt";
  global.connection = await MongoClient.connect(process.env.MONGO_URL, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  });
  const testDB = connection.db();
  await testDB.collection("users").createIndex({ email: 1 }, { unique: true });
  testDB.collection("users").updateOne(
    { email: "student@example.com" },
    {
      $set: {
        firstName: "Student",
        lastName: "Student",
        email: "student@example.com",
        role: "student",
        password: hashSync("stPass", 10),
        extraTime: 20,
      },
    },
    {
      upsert: true,
    }
  );
  testDB.collection("users").updateOne(
    { email: "professor@example.com" },
    {
      $set: {
        firstName: "Professor",
        lastName: "Professor",
        email: "professor@example.com",
        role: "professor",
        password: hashSync("prPass", 10),
      },
    },
    {
      upsert: true,
    }
  );
  testDB.collection("users").updateOne(
    { email: "admin@example.com" },
    {
      $set: {
        firstName: "Admin",
        lastName: "Admin",
        email: "admin@example.com",
        role: "admin",
        password: hashSync("adPass", 10),
      },
    },
    {
      upsert: true,
    }
  );

  global.app = build(true);
  global.app.register(require("fastify-mongodb"), { url: process.env.MONGO_URL });
});

/**
 * Close the mock database and the server after all tests
 */
afterAll(async () => {
  await global.connection.close();
  global.app.close();
});