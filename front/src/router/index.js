import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Exams from '../views/Exams'
import TakeExam from '../components/Exams/TakeExam'
import Questions from '../components/Exams/Questions'
import Admin from '../views/Admin'

Vue.use(VueRouter)

const routes = [{
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/exams',
    name: 'Exams',
    component: Exams
  },
  {
    path: '/exam/:exam_id',
    name: 'Exam',
    component: TakeExam
  },
  {
    path: '/exam/questions/:exam_id',
    name: 'Questions',
    component: Questions
  },
  {
    path: '/admin',
    name: 'Admin',
    component: Admin
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router