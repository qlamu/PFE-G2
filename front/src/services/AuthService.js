import axios from "axios";

export default new (class AuthService {
  async register(firstName, lastName, email, password) {
    const resp = axios.post(`${process.env.VUE_APP_SERVER}/register`, {
      firstName: firstName,
      lastName: lastName,
      email: email,
      password: password,
    });
    return (await resp).data;
  }

  async login(email, password) {
    const resp = axios.post(`${process.env.VUE_APP_SERVER}/login`, {
      email: email,
      password: password,
    });

    const json = (await resp).data;
    if (json.data?.jwt) {
      axios.defaults.headers.common = {
        Authorization: `Bearer ${json.data.jwt}`,
      };
      localStorage.setItem("jwt", json.data.jwt);
    }

    if (json.data?.user) {
      localStorage.setItem("user", JSON.stringify(json.data.user));
    }

    return json;
  }

  signOut() {
    axios.defaults.headers.common = {};
    localStorage.removeItem("jwt");
  }
})();
