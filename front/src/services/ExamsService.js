import axios from "axios";

export default new (class ExamsService {
  async getExams() {
    const resp = axios.get(`${process.env.VUE_APP_SERVER}/exam/`);
    return (await resp).data;
  }

  async createExam(exam) {
    const resp = axios.put(`${process.env.VUE_APP_SERVER}/exam/create`, exam);
    return (await resp).data;
  }

  async patchExam(idExam, newBody) {
    const resp = axios.patch(`${process.env.VUE_APP_SERVER}/exam/update/${idExam}`, newBody);
    return (await resp).data;
  }

  async deleteExam(idExam) {
    const resp = axios.delete(`${process.env.VUE_APP_SERVER}/exam/delete/${idExam}`);
    return (await resp).data;
  }

  async getExam(idExam) {
    const resp = axios.get(`${process.env.VUE_APP_SERVER}/exam/${idExam}`);
    return (await resp).data;
  }

  async updateTimeAway(idExam, body) {
    const resp = axios.post(`${process.env.VUE_APP_SERVER}/exam/timeAway/${idExam}`, body);
    return (await resp).data;
  }
})();
