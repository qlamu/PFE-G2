import axios from "axios";

export default new (class AdminService {
  async getUsers() {
    const resp = axios.get(`${process.env.VUE_APP_SERVER}/admin/users`);
    return (await resp).data;
  }

  async patchUser(userID, newUserInfo) {
    const resp = axios.patch(`${process.env.VUE_APP_SERVER}/admin/users/update/${userID}`, {
      ...newUserInfo,
    });

    return (await resp).data;
  }
})();
