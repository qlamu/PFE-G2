import axios from "axios";

export default new (class AnswersService {
  async putAnswer(idExam, idQuestion, answers) {
    const resp = axios.put(`${process.env.VUE_APP_SERVER}/exam/answers/${idExam}/${idQuestion}`, answers);
    return (await resp).data;
  }
})();
