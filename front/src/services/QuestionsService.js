import axios from "axios";

export default new (class QuestionsService {
  async createQuestion(idExam, newQuestion) {
    const resp = axios.put(`${process.env.VUE_APP_SERVER}/exam/questions/${idExam}`, newQuestion);
    return (await resp).data;
  }

  async deleteQuestion(idExam, idQuestion) {
    const resp = axios.delete(
      `${process.env.VUE_APP_SERVER}/exam/questions/delete/${idExam}/${idQuestion}`
    );
    return (await resp).data;
  }

  async patchQuestion(idExam, idQuestion, newQuestion) {
    const resp = axios.patch(
      `${process.env.VUE_APP_SERVER}/exam/questions/update/${idExam}/${idQuestion}`,
      newQuestion
    );
    return (await resp).data;
  }
})();
