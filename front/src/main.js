import Vue from 'vue'
import App from './App.vue'
import router from './router'
import vuetify from './plugins/vuetify';
import 'material-design-icons-iconfont/dist/material-design-icons.css'
import axios from "axios";
import VueSession from "vue-session";
import DatetimePicker from 'vuetify-datetime-picker'
//bus
export const bus = new Vue();

Vue.use(VueSession);
Vue.use(DatetimePicker)
Vue.config.productionTip = false;

axios.defaults.headers.common = {
  "Content-Type": "application/json",
};
axios.defaults.timeout = 10000;
axios.interceptors.response.use(
  function(response) {
    return response;
  },
  function(error) {
    if (error.response?.status == 401) {
      axios.defaults.headers.common = {
        Authorization: "",
      };
      router.push("/");
    }
    return Promise.reject(error);
  }
);
if (localStorage.getItem("jwt") !== null) {
  axios.defaults.headers.common = {
    Authorization: `Bearer ${localStorage.getItem("jwt")}`,
  };
}

new Vue({
  router,
  vuetify,
  render: h => h(App)
}).$mount('#app')
